class RPNCalculator
  OPS = ["+", "-", "*", "/"]

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    raise "calculator is empty" if @stack.length < 2
    sum = @stack.pop + @stack.pop
    @stack << sum
  end
    #
  def minus
    raise "calculator is empty" if @stack.length < 2
    neg_diff = @stack.pop - @stack.pop
    @stack << -neg_diff
  end

  def divide
    raise "calculator is empty" if @stack.length < 2
    result = 1/ (@stack.pop.to_f / @stack.pop)
    @stack << result
  end

  def times
    raise "calculator is empty" if @stack.length < 2
    product = @stack.pop * @stack.pop
    @stack << product
  end

  def value
    raise "calculator is empty" if @stack.length < 1
    @stack[-1]
  end

  def tokens str
    tokenized = str.split.map { |token| OPS.include?(token) ? token.to_sym : token.to_i }
    tokenized
  end

  def do_math stack
    raise "calculator is empty" if @stack.length < 2
    second_term = @stack.pop
    first_term = @stack.pop
  end

end
